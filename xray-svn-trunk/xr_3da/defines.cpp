#include "stdafx.h"

#ifdef DEBUG
	ECORE_API BOOL bDebug	= FALSE;
	
#endif

// Video
//. u32			psCurrentMode		= 1024;
u32			psCurrentVidMode[2] = {1024,768};
u32			psCurrentBPP		= 32;
// release version always has "mt_*" enabled
Flags32		psDeviceFlags		= {rsFullscreen|rsDetails|mtPhysics|mtSound|mtNetwork|rsDrawStatic|rsDrawDynamic|rsRefresh60hz|rsPrefObjects};
#pragma todo("this is not the best place for this, but in level.h is also not. maybe will move it to stdafx in xrGame")
Flags32		g_uCommonFlags		= { CF_AiUseTorchDynamicLights | CF_SkipTextureLoading | CF_Prefetch_UI };

// textures 
int			psTextureLOD		= 1;
