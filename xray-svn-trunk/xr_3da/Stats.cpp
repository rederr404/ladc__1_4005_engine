#include "stdafx.h"
#include "GameFont.h"
#pragma hdrstop

#include "../xrcdb/ISpatial.h"
#include "IGame_Persistent.h"
#include "render.h"
#include "xr_object.h"

#include "ConstantDebug.h"

#include "../Include/xrRender/DrawUtils.h"

int		g_ErrorLineCount	= 15;
Flags32 g_stats_flags		= {0};

// stats
DECLARE_RP(Stats);

class	optimizer	{
	float	average_	;
	BOOL	enabled_	;
public:
	optimizer	()		{
		average_	= 30.f;
//		enabled_	= TRUE;
//		disable		();
		// because Engine is not exist
		enabled_	= FALSE;
	}

	BOOL	enabled	()	{ return enabled_;	}
	void	enable	()	{ if (!enabled_)	{ Engine.External.tune_resume	();	enabled_=TRUE;	}}
	void	disable	()	{ if (enabled_)		{ Engine.External.tune_pause	();	enabled_=FALSE; }}
	void	update	(float value)	{
		if (value < average_*0.7f)	{
			// 25% deviation
			enable	();
		} else {
			disable	();
		};
		average_	= 0.99f*average_ + 0.01f*value;
	};
};
static	optimizer	vtune;

BOOL			g_bDisableRedText	= FALSE;

CStats::CStats	()
{
	fFPS				= 30.f;
	fRFPS				= 30.f;
	fTPS				= 0;
	pFont				= 0;

	fMem_calls			= 0;
	RenderDUMP_DT_Count = 0;
	Device.seqRender.Add		(this,REG_PRIORITY_LOW-1000);
	UpdateFPSCounterSkip= 0.f;
	UpdateFPSMinute		= 0.f;
	fCounterTotalMinute = 0.f;
	iTotalMinute		= -1;
	iAvrageMinute		= -1;
	iFPS				= -1;
	pFPSFont			= 0;

	occ_pool_size		= 0;
	occ_used_size		= 0;
	occ_freed_ids_size	= 0;
	occ_Begin_calls		= 0;
	occ_End_calls		= 0;
	occ_Get_calls		= 0;
}

CStats::~CStats()
{
	Device.seqRender.Remove		(this);
	xr_delete		(pFont);
	xr_delete		(pFPSFont);
}

void _draw_cam_pos(CGameFont* pFont)
{
	float sz		= pFont->GetHeight();
	pFont->SetHeightI(0.02f);
	pFont->SetColor	(0xffffffff);
	pFont->Out		(10, 600, "CAMERA POSITION:  [%3.2f,%3.2f,%3.2f]",VPUSH(Device.vCameraPosition));
	pFont->SetHeight(sz);
	pFont->OnRender	();
}

extern BOOL show_FPS_only;
extern BOOL show_engine_timers;
extern BOOL show_render_times;
extern BOOL log_render_times;

void CStats::Show() 
{
	// Stop timers
	{
		EngineTOTAL.FrameEnd		();	
		Sheduler.FrameEnd			();	
		UpdateClient.FrameEnd		();	
		Physics.FrameEnd			();	
		ph_collision.FrameEnd		();
		ph_core.FrameEnd			();
		Animation.FrameEnd			();	
		AI_Think.FrameEnd			();
		AI_Range.FrameEnd			();
		AI_Path.FrameEnd			();
		AI_Node.FrameEnd			();
		AI_Vis.FrameEnd				();
		AI_Vis_Query.FrameEnd		();
		AI_Vis_RayTests.FrameEnd	();
		
		RenderTOTAL.FrameEnd		();
		RenderCALC.FrameEnd			();
		RenderCALC_HOM.FrameEnd		();
		RenderDUMP.FrameEnd			();	
		RenderDUMP_RT.FrameEnd		();
		RenderDUMP_SKIN.FrameEnd	();	
		RenderDUMP_Wait.FrameEnd	();	
		RenderDUMP_Wait_S.FrameEnd	();	
		RenderDUMP_HUD.FrameEnd		();	
		RenderDUMP_Glows.FrameEnd	();	
		RenderDUMP_Lights.FrameEnd	();	
		RenderDUMP_WM.FrameEnd		();	
		RenderDUMP_DT_VIS.FrameEnd	();	
		RenderDUMP_DT_Render.FrameEnd();	
		RenderDUMP_DT_Cache.FrameEnd();
		RenderDUMP_Pcalc.FrameEnd	();	
		RenderDUMP_Scalc.FrameEnd	();	
		RenderDUMP_Srender.FrameEnd	();	
		
		Sound.FrameEnd				();
		Input.FrameEnd				();
		clRAY.FrameEnd				();	
		clBOX.FrameEnd				();
		clFRUSTUM.FrameEnd			();
		
		netClient1.FrameEnd			();
		netClient2.FrameEnd			();
		netServer.FrameEnd			();

		netClientCompressor.FrameEnd();
		netServerCompressor.FrameEnd();
		
		TEST0.FrameEnd				();
		TEST1.FrameEnd				();
		TEST2.FrameEnd				();
		TEST3.FrameEnd				();

		g_SpatialSpace->stat_insert.FrameEnd		();
		g_SpatialSpace->stat_remove.FrameEnd		();
		g_SpatialSpacePhysic->stat_insert.FrameEnd	();
		g_SpatialSpacePhysic->stat_remove.FrameEnd	();
	}

	// calc FPS & TPS
	if (Device.fTimeDelta>EPS_S) {
		float fps  = 1.f/Device.fTimeDelta;
		//if (Engine.External.tune_enabled)	vtune.update	(fps);
		float fOne = 0.3f;
		float fInv = 1.f-fOne;
		fFPS = fInv*fFPS + fOne*fps;

		if (RenderTOTAL.result>EPS_S) {
			u32	rendered_polies = Device.m_pRender->GetCacheStatPolys();
			fTPS = fInv*fTPS + fOne*float(rendered_polies)/(RenderTOTAL.result*1000.f);
			//fTPS = fInv*fTPS + fOne*float(RCache.stat.polys)/(RenderTOTAL.result*1000.f);
			fRFPS= fInv*fRFPS+ fOne*1000.f/RenderTOTAL.result;
		}
	}
	{
		float mem_count		= float	(Memory.stat_calls);
		if (mem_count>fMem_calls)	fMem_calls	=	mem_count;
		else						fMem_calls	=	.9f*fMem_calls + .1f*mem_count;
		Memory.stat_calls	= 0		;
	}

	int frm = 2000;
	div_t ddd = div(Device.dwFrame,frm);
	if( ddd.rem < frm/2.0f ){
		pFont->SetColor	(0xFFFFFFFF	);
		pFont->OutSet	(0,0);
		pFont->OutNext	(*eval_line_1);
		pFont->OutNext	(*eval_line_2);
		pFont->OutNext	(*eval_line_3);
		pFont->OnRender	();
	}

	CGameFont& F = *pFont;
	float		f_base_size	= 0.01f;
				F.SetHeightI	(f_base_size);

	if (vtune.enabled())	{
		float sz		= pFont->GetHeight();
		pFont->SetHeightI(0.02f);
		pFont->SetColor	(0xFFFF0000	);
		pFont->OutSet	(Device.dwWidth/2.0f+(pFont->SizeOf_("--= tune =--")/2.0f),Device.dwHeight/2.0f);
		pFont->OutNext	("--= tune =--");
		pFont->OnRender	();
		pFont->SetHeight(sz);
	};

	// Show 
	if (psDeviceFlags.test(rsStatistic) || show_FPS_only == TRUE || show_engine_timers == TRUE || show_render_times == TRUE)
	{
		if (psDeviceFlags.test(rsStatistic))
		{
			static float	r_ps = 0;
			static float	b_ps = 0;
			r_ps = .99f*r_ps + .01f*(clRAY.count / clRAY.result);
			b_ps = .99f*b_ps + .01f*(clBOX.count / clBOX.result);

			CSound_stats				snd_stat;
			::Sound->statistic(&snd_stat, 0);
			F.SetColor(0xFFFFFFFF);

			F.OutSet(0, 0);
			F.OutNext("FPS/RFPS:    %3.1f/%3.1f", fFPS, fRFPS);
			F.OutNext("TPS:         %2.2f M", fTPS);
			m_pRender->OutData1(F);
			//F.OutNext	("VERT:        %d/%d",		RCache.stat.verts,RCache.stat.calls?RCache.stat.verts/RCache.stat.calls:0);
			//F.OutNext	("POLY:        %d/%d",		RCache.stat.polys,RCache.stat.calls?RCache.stat.polys/RCache.stat.calls:0);
			//F.OutNext	("DIP/DP:      %d",			RCache.stat.calls);

#ifdef FS_DEBUG
			F.OutNext	("mapped:      %d",			g_file_mapped_memory);
			F.OutSkip	();
#endif
			m_pRender->OutData3(F);
			//F.OutNext	("xforms:      %d",			RCache.stat.xforms);
			F.OutSkip();

#define PPP(a) (100.f*float(a)/float(EngineTOTAL.result))
			F.OutNext("*** ENGINE:  %2.2fms", EngineTOTAL.result);
			F.OutNext("Memory:      %2.2fa", fMem_calls);
			F.OutNext("uClients:    %2.2fms, %2.1f%%, crow(%d)/active(%d)/total(%d)", UpdateClient.result, PPP(UpdateClient.result), UpdateClient_crows, UpdateClient_active, UpdateClient_total);
			F.OutNext("uSheduler:   %2.2fms, %2.1f%%", Sheduler.result, PPP(Sheduler.result));
			F.OutNext("uSheduler_L: %2.2fms", fShedulerLoad);
			F.OutNext("uParticles:  Qstart[%d] Qactive[%d] Qdestroy[%d]", Particles_starting, Particles_active, Particles_destroy);
			F.OutNext("spInsert:    o[%.2fms, %2.1f%%], p[%.2fms, %2.1f%%]", g_SpatialSpace->stat_insert.result, PPP(g_SpatialSpace->stat_insert.result), g_SpatialSpacePhysic->stat_insert.result, PPP(g_SpatialSpacePhysic->stat_insert.result));
			F.OutNext("spRemove:    o[%.2fms, %2.1f%%], p[%.2fms, %2.1f%%]", g_SpatialSpace->stat_remove.result, PPP(g_SpatialSpace->stat_remove.result), g_SpatialSpacePhysic->stat_remove.result, PPP(g_SpatialSpacePhysic->stat_remove.result));
			F.OutNext("Physics:     %2.2fms, %2.1f%%", Physics.result, PPP(Physics.result));
			F.OutNext("  collider:  %2.2fms", ph_collision.result);
			F.OutNext("  solver:    %2.2fms, %d", ph_core.result, ph_core.count);
			F.OutNext("aiThink:     %2.2fms, %d", AI_Think.result, AI_Think.count);
			F.OutNext("  aiRange:   %2.2fms, %d", AI_Range.result, AI_Range.count);
			F.OutNext("  aiPath:    %2.2fms, %d", AI_Path.result, AI_Path.count);
			F.OutNext("  aiNode:    %2.2fms, %d", AI_Node.result, AI_Node.count);
			F.OutNext("aiVision:    %2.2fms, %d", AI_Vis.result, AI_Vis.count);
			F.OutNext("  Query:     %2.2fms", AI_Vis_Query.result);
			F.OutNext("  RayCast:   %2.2fms", AI_Vis_RayTests.result);
			F.OutSkip();

#undef  PPP
#define PPP(a) (100.f*float(a)/float(RenderTOTAL.result))
			F.OutNext("*** RENDER:  %2.2fms", RenderTOTAL.result);
			F.OutNext("R_CALC:      %2.2fms, %2.1f%%", RenderCALC.result, PPP(RenderCALC.result));
			F.OutNext("  HOM:       %2.2fms, %d", RenderCALC_HOM.result, RenderCALC_HOM.count);
			F.OutNext("  Skeletons: %2.2fms, %d", Animation.result, Animation.count);
			F.OutNext("R_DUMP:      %2.2fms, %2.1f%%", RenderDUMP.result, PPP(RenderDUMP.result));
			F.OutNext("  Wait-L:    %2.2fms", RenderDUMP_Wait.result);
			F.OutNext("  Wait-S:    %2.2fms", RenderDUMP_Wait_S.result);
			F.OutNext("  Skinning:  %2.2fms", RenderDUMP_SKIN.result);
			F.OutNext("  DT_Vis/Cnt:%2.2fms/%d", RenderDUMP_DT_VIS.result, RenderDUMP_DT_Count);
			F.OutNext("  DT_Render: %2.2fms", RenderDUMP_DT_Render.result);
			F.OutNext("  DT_Cache:  %2.2fms", RenderDUMP_DT_Cache.result);
			F.OutNext("  Wallmarks: %2.2fms, %d/%d - %d", RenderDUMP_WM.result, RenderDUMP_WMS_Count, RenderDUMP_WMD_Count, RenderDUMP_WMT_Count);
			F.OutNext("  Glows:     %2.2fms", RenderDUMP_Glows.result);
			F.OutNext("  Lights:    %2.2fms, %d", RenderDUMP_Lights.result, RenderDUMP_Lights.count);
			F.OutNext("  RT:        %2.2fms, %d", RenderDUMP_RT.result, RenderDUMP_RT.count);
			F.OutNext("  HUD:       %2.2fms", RenderDUMP_HUD.result);
			F.OutNext("  P_calc:    %2.2fms", RenderDUMP_Pcalc.result);
			F.OutNext("  S_calc:    %2.2fms", RenderDUMP_Scalc.result);
			F.OutNext("  S_render:  %2.2fms, %d", RenderDUMP_Srender.result, RenderDUMP_Srender.count);
			F.OutSkip();
			F.OutNext("*** SOUND:   %2.2fms", Sound.result);
			F.OutNext("  TGT/SIM/E: %d/%d/%d", snd_stat._rendered, snd_stat._simulated, snd_stat._events);
			F.OutNext("  HIT/MISS:  %d/%d", snd_stat._cache_hits, snd_stat._cache_misses);
			F.OutSkip();
			F.OutNext("Input:       %2.2fms", Input.result);
			F.OutNext("clRAY:       %2.2fms, %d, %2.0fK", clRAY.result, clRAY.count, r_ps);
			F.OutNext("clBOX:       %2.2fms, %d, %2.0fK", clBOX.result, clBOX.count, b_ps);
			F.OutNext("clFRUSTUM:   %2.2fms, %d", clFRUSTUM.result, clFRUSTUM.count);
			F.OutSkip();
			F.OutNext("netClientRecv:   %2.2fms, %d", netClient1.result, netClient1.count);
			F.OutNext("netClientSend:   %2.2fms, %d", netClient2.result, netClient2.count);
			F.OutNext("netServer:   %2.2fms, %d", netServer.result, netServer.count);
			F.OutNext("netClientCompressor:   %2.2fms", netClientCompressor.result);
			F.OutNext("netServerCompressor:   %2.2fms", netServerCompressor.result);

			F.OutSkip();

			F.OutSkip();
			F.OutNext("TEST 0:      %2.2fms, %d", TEST0.result, TEST0.count);
			F.OutNext("TEST 1:      %2.2fms, %d", TEST1.result, TEST1.count);
			F.OutNext("TEST 2:      %2.2fms, %d", TEST2.result, TEST2.count);
			F.OutNext("TEST 3:      %2.2fms, %d", TEST3.result, TEST3.count);
#ifdef DEBUG_MEMORY_MANAGER
			F.OutSkip	();
			F.OutNext	("str: cmp[%3d], dock[%3d], qpc[%3d]",Memory.stat_strcmp,Memory.stat_strdock,CPU::qpc_counter);
			Memory.stat_strcmp	=	0		;
			Memory.stat_strdock	=	0		;
			CPU::qpc_counter	=	0		;
#else
			F.OutSkip();
			F.OutNext("qpc[%3d]", CPU::qpc_counter);
			CPU::qpc_counter = 0;
#endif
			F.OutSkip();
			m_pRender->OutData4(F);
			/*
			F.OutNext	("static:        %3.1f/%d",	RCache.stat.r.s_static.verts/1024.f,		RCache.stat.r.s_static.dips );
			F.OutNext	("flora:         %3.1f/%d",	RCache.stat.r.s_flora.verts/1024.f,			RCache.stat.r.s_flora.dips );
			F.OutNext	("  flora_lods:  %3.1f/%d",	RCache.stat.r.s_flora_lods.verts/1024.f,	RCache.stat.r.s_flora_lods.dips );
			F.OutNext	("dynamic:       %3.1f/%d",	RCache.stat.r.s_dynamic.verts/1024.f,		RCache.stat.r.s_dynamic.dips );
			F.OutNext	("  dynamic_sw:  %3.1f/%d",	RCache.stat.r.s_dynamic_sw.verts/1024.f,	RCache.stat.r.s_dynamic_sw.dips );
			F.OutNext	("  dynamic_inst:%3.1f/%d",	RCache.stat.r.s_dynamic_inst.verts/1024.f,	RCache.stat.r.s_dynamic_inst.dips );
			F.OutNext	("  dynamic_1B:  %3.1f/%d",	RCache.stat.r.s_dynamic_1B.verts/1024.f,	RCache.stat.r.s_dynamic_1B.dips );
			F.OutNext	("  dynamic_2B:  %3.1f/%d",	RCache.stat.r.s_dynamic_2B.verts/1024.f,	RCache.stat.r.s_dynamic_2B.dips );
			F.OutNext	("details:       %3.1f/%d",	RCache.stat.r.s_details.verts/1024.f,		RCache.stat.r.s_details.dips );
			*/
			//////////////////////////////////////////////////////////////////////////
			// Renderer specific
			F.SetHeightI(f_base_size);
			F.OutSet(200, 0);
			Render->Statistics(&F);

			//////////////////////////////////////////////////////////////////////////
			// Game specific
			F.SetHeightI(f_base_size);
			F.OutSet(400, 0);
			g_pGamePersistent->Statistics(&F);

			//////////////////////////////////////////////////////////////////////////
			// process PURE STATS
			F.SetHeightI(f_base_size);
			seqStats.Process(rp_Stats);
			pFont->OnRender();
#ifdef LA_SHADERS_DEBUG
			/////////////////////////////////////////////////////////////////////////
			// shaders constants debug
			F.SetHeightI						(f_base_size);
			F.OutSet							(1500,300);
			g_pConstantsDebug->OnRender			(pFont);
			pFont->OnRender						();
#endif
		}
		if (show_FPS_only == TRUE || show_engine_timers == TRUE || show_render_times == TRUE)
		{
			CGameFont& fpsFont = *pFPSFont;

			fpsFont.SetColor(0xFF8772A8);
			fpsFont.SetHeightI(0.02);
			fpsFont.SetAligment(CGameFont::alRight);
			float posx, posy;
			if (Device.dwWidth / Device.dwHeight < 1.5f){			//posx = 1200.f * (Device.dwWidth / 1280.f);
																	posx = 1180.f * (Device.dwWidth / 1280.f);
																	posy = 10.f * (Device.dwHeight / 720.f);
			}
			else{
																	//posx = 940.f * (Device.dwWidth / 1024.f);
																	posx = 890.f * (Device.dwWidth / 1024.f);
																	posy = 15.f * (Device.dwHeight / 768.f);
																	fpsFont.SetHeightI(0.030);
			}
			fpsFont.OutSet(posx, posy);

			if (show_FPS_only == TRUE){
				UpdateFPSCounterSkip += 1.f *Device.fTimeDelta;
				UpdateFPSMinute += 1.f *Device.fTimeDelta;
				if (UpdateFPSCounterSkip > 0.1f){
					UpdateFPSCounterSkip = 0;
					iFPS = fFPS;
					fCounterTotalMinute += fFPS;
				}
				if (UpdateFPSMinute > 60.0f){
					UpdateFPSMinute = 0;
					iTotalMinute = fCounterTotalMinute / 10;
					iAvrageMinute = fCounterTotalMinute / 600;
					fCounterTotalMinute = 0;

				}
				fpsFont.OutNext("      %i", iFPS);
				fpsFont.OutSkip();
				fpsFont.OutNext("A/MIN %i", iAvrageMinute);
				fpsFont.OutNext("T/MIN %i", iTotalMinute);
			}
			if (show_engine_timers == TRUE){
				fpsFont.OutSkip();
				fpsFont.OutNext("---Engine timers");
				fpsFont.OutSkip();
				fpsFont.OutNext("%2.2fms = Engine Time", EngineTOTAL.result);
				fpsFont.OutNext("%2.2fms = Render Time", RenderTOTAL.result);
				fpsFont.OutNext("%fms = Main Thread Time", MainThreadTime);
				fpsFont.OutNext("%fms = Aux. Thread#1 Time", Thread1Time);
				fpsFont.OutNext("%i = MainThreadWaitedCounter", MainThreadSuspendCounter);
				fpsFont.OutNext("%u = Thread#1 Objects Computed", obects);
				fpsFont.OutSkip();
				fpsFont.OutNext("uClients:    %2.2fms", UpdateClient.result);
				fpsFont.OutNext("uSheduler:   %2.2fms", Sheduler.result);

				//Logging
				if (1 == 2){
					Msg("%2.2fms = Engine Time", EngineTOTAL.result);
				}
			}

			if (show_render_times == TRUE){
				fpsFont.OutSkip();
				fpsFont.OutNext("CRender::Render(): (Reffer to r3/r4_R_render.cpp)");
				fpsFont.OutNext("%i = frame#", MeasuredframeNo);
				fpsFont.OutSkip();
				fpsFont.OutNext("%fms = Stage1", Stage1Time);
				fpsFont.OutNext("%fms = Stage2", Stage2Time);
				fpsFont.OutNext("%fms = CPU_wait_GPU", CPU_wait_GPUtime);
				fpsFont.OutNext("%fms = RenderMain", RenderMainTime);
				fpsFont.OutNext("%fms = PART0", PART0Time);
				fpsFont.OutNext("%fms = ComplexStage1", ComplexStage1Time);
				fpsFont.OutNext("%fms = PART1", PART1Time);
				fpsFont.OutNext("%fms = RenderHudUiT", RenderHudUiTime);
				fpsFont.OutNext("%fms = Wallmarks", WallmarksTime);
				fpsFont.OutNext("%fms = SmapVisSolver", SmapVisSolverTime);
				fpsFont.OutNext("%fms = MSAAEdges", MSAAEdgesTime);
				fpsFont.OutNext("%fms = WetSurfaces", WetSurfacesTime);
				fpsFont.OutNext("%fms = Sun", SunTime);
				fpsFont.OutNext("%fms = Lights1", Lights1Time);
				fpsFont.OutNext("%fms = Lights2", Lights2Time);
				fpsFont.OutNext("%fms = TargetCombine", TargetCombineTime);
				fpsFont.OutNext("%fms = RenderTotal", RenderTotalTime);
				fpsFont.OutSkip();
				fpsFont.OutNext("Occlusion Debug:");
				fpsFont.OutNext("%u = Occ_pool_size", occ_pool_size);
				fpsFont.OutNext("%u = Occ_used_size", occ_used_size);
				fpsFont.OutNext("%u = Occ_free_size", occ_freed_ids_size);

				if (MeasuredframeNo >= 32700)MeasuredframeNo = 0;

				//Logging
				if (log_render_times){
					Msg("CRender::Render(): (Reffer to r3/r4_R_render.cpp); frame# = %i", MeasuredframeNo);
					Msg("%fms = Stage1", Stage1Time);
					Msg("%fms = Stage2", Stage2Time);
					Msg("%fms = CPU_wait_GPU", CPU_wait_GPUtime);
					Msg("%fms = RenderMain", RenderMainTime);
					Msg("%fms = PART0", PART0Time);
					Msg("%fms = ComplexStage1", ComplexStage1Time);
					Msg("%fms = PART1", PART1Time);
					Msg("%fms = RenderHudUi", RenderHudUiTime);
					Msg("%fms = Wallmarks", WallmarksTime);
					Msg("%fms = SmapVisSolver", SmapVisSolverTime);
					Msg("%fms = MSAAEdges", MSAAEdgesTime);
					Msg("%fms = WetSurfaces", WetSurfacesTime);
					Msg("%fms = Sun", SunTime);
					Msg("%fms = Lights1", Lights1Time);
					Msg("%fms = Lights2", Lights2Time);
					Msg("%fms = TargetCombine", TargetCombineTime);
					Msg("%fms = RenderTotal", RenderTotalTime);

					Msg("Occlusion Debug:");
					Msg("%u = Occ_Bgn_calls", occ_Begin_calls);
					Msg("%u = Occ_End_calls", occ_End_calls);
					Msg("%u = Occ_Get_calls", occ_Get_calls);
					occ_Begin_calls = occ_End_calls = occ_Get_calls = 0;
				}
			}

			pFPSFont->OnRender();
		}
	};

	if(psDeviceFlags.test(rsCameraPos)){
		_draw_cam_pos					(pFont);
		pFont->OnRender					();
	};

	{
		EngineTOTAL.FrameStart		();	
		Sheduler.FrameStart			();	
		UpdateClient.FrameStart		();	
		Physics.FrameStart			();	
		ph_collision.FrameStart		();
		ph_core.FrameStart			();
		Animation.FrameStart		();	
		AI_Think.FrameStart			();
		AI_Range.FrameStart			();
		AI_Path.FrameStart			();
		AI_Node.FrameStart			();
		AI_Vis.FrameStart			();
		AI_Vis_Query.FrameStart		();
		AI_Vis_RayTests.FrameStart	();
		
		RenderTOTAL.FrameStart		();
		RenderCALC.FrameStart		();
		RenderCALC_HOM.FrameStart	();
		RenderDUMP.FrameStart		();	
		RenderDUMP_RT.FrameStart	();
		RenderDUMP_SKIN.FrameStart	();	
		RenderDUMP_Wait.FrameStart	();	
		RenderDUMP_Wait_S.FrameStart();	
		RenderDUMP_HUD.FrameStart	();	
		RenderDUMP_Glows.FrameStart	();	
		RenderDUMP_Lights.FrameStart();	
		RenderDUMP_WM.FrameStart	();	
		RenderDUMP_DT_VIS.FrameStart();	
		RenderDUMP_DT_Render.FrameStart();	
		RenderDUMP_DT_Cache.FrameStart();	
		RenderDUMP_Pcalc.FrameStart	();	
		RenderDUMP_Scalc.FrameStart	();	
		RenderDUMP_Srender.FrameStart();	
		
		Sound.FrameStart			();
		Input.FrameStart			();
		clRAY.FrameStart			();	
		clBOX.FrameStart			();
		clFRUSTUM.FrameStart		();
		
		netClient1.FrameStart		();
		netClient2.FrameStart		();
		netServer.FrameStart		();
		netClientCompressor.FrameStart();
		netServerCompressor.FrameStart();

		TEST0.FrameStart			();
		TEST1.FrameStart			();
		TEST2.FrameStart			();
		TEST3.FrameStart			();

		g_SpatialSpace->stat_insert.FrameStart		();
		g_SpatialSpace->stat_remove.FrameStart		();

		g_SpatialSpacePhysic->stat_insert.FrameStart();
		g_SpatialSpacePhysic->stat_remove.FrameStart();
	}
	dwSND_Played = dwSND_Allocated = 0;
	Particles_starting = Particles_active = Particles_destroy = 0;
}

void	_LogCallback				(LPCSTR string)
{
	if (string && '!'==string[0] && ' '==string[1])
		Device.Statistic->errors.push_back	(shared_str(string));
}

void CStats::OnDeviceCreate			()
{
	g_bDisableRedText				= strstr(Core.Params,"-xclsx")?TRUE:FALSE;

	pFont	= new CGameFont("stat_font", CGameFont::fsDeviceIndependent);
	pFPSFont = new CGameFont("hud_font_di2", CGameFont::fsDeviceIndependent);

	if(!pSettings->section_exist("evaluation")
		||!pSettings->line_exist("evaluation","line1")
		||!pSettings->line_exist("evaluation","line2")
		||!pSettings->line_exist("evaluation","line3") )
		FATAL	("CStats::OnDeviceCreate()");

	eval_line_1 = pSettings->r_string_wb("evaluation","line1");
	eval_line_2 = pSettings->r_string_wb("evaluation","line2");
	eval_line_3 = pSettings->r_string_wb("evaluation","line3");

	Thread2Time = -1.f;
	Thread1Time = -1.f;
	MainThreadTime = -1.f;
	MainThreadSuspendCounter = 0;

	Stage1Time = -1.f;
	Stage2Time = -1.f;
	CPU_wait_GPUtime = -1.f;
	RenderMainTime = -1.f;
	PART0Time = -1.f;
	ComplexStage1Time = -1.f;
	PART1Time = -1.f;
	RenderHudUiTime = -1.f;
	WallmarksTime = -1.f;
	SmapVisSolverTime = -1.f;
	MSAAEdgesTime = -1.f;
	WetSurfacesTime = -1.f;
	SunTime = -1.f;
	Lights1Time = -1.f;
	Lights2Time = -1.f;
	TargetCombineTime = -1.f;
	RenderTotalTime = -1.f;
	MeasuredframeNo = 0;

#ifdef LA_SHADERS_DEBUG
	g_pConstantsDebug = new CConstantsDebug();
#endif
}

void CStats::OnDeviceDestroy		()
{
	SetLogCB(NULL);
	xr_delete	(pFont);
	xr_delete	(pFPSFont);
#ifdef LA_SHADERS_DEBUG
	xr_delete	(g_pConstantsDebug);
#endif
}

void CStats::OnRender				()
{

}
